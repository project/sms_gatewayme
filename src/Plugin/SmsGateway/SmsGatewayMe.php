<?php

namespace Drupal\sms_gatewayme\Plugin\SmsGateway;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\sms\Plugin\SmsGatewayPluginBase;
use Drupal\sms\Message\SmsMessageInterface;
use Drupal\sms\Message\SmsMessageResult;
use Drupal\sms\Message\SmsMessageReportStatus;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sms\Message\SmsDeliveryReport;
use Drupal\sms\Message\SmsMessageResultStatus;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\sms\Direction;
use Drupal\sms\Entity\SmsGatewayInterface;
use Drupal\sms\Message\SmsMessage;
use Drupal\sms\SmsProcessingResponse;
use SMSGatewayMe\Client\ApiClient;
use SMSGatewayMe\Client\Configuration;
use SMSGatewayMe\Client\Api\MessageApi;
use SMSGatewayMe\Client\Model\SendMessageRequest;

/**
 * Defines a gateway for sending SMS with smsgateway.me.
 *
 * @SmsGateway(
 *   id = "smsgatewayme",
 *   label = @Translation("SMS Gateway (dot) Me"),
 *   outgoing_message_max_recipients = -1,
 *   incoming = TRUE,
 *   incoming_route = TRUE,
 * )
 */
class SmsGatewayMe extends SmsGatewayPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Construct a new SmsGateway plugin.
   *
   * @param array $configuration
   *   The configuration to use and build the sms gateway.
   * @param string $plugin_id
   *   The gateway id.
   * @param mixed $plugin_definition
   *   The gateway plugin definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger_factory->get('sms_gatewayme');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')
    );
  }

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'device' => '',
      'key' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['device_id'] = [
      '#type' => 'textfield',
      '#title' => t('Device ID'),
      '#default_value' => $config['device_id'],
      '#description' => t('The device ID that this gateway will communicate with.'),
    ];
    $form['key'] = [
      '#type' => 'textarea',
      '#title' => t('The gateway API key.'),
      '#default_value' => $config['key'],
      '#description' => t('Your API key, from your account page at the website.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['key'] = $form_state->getValue('key');
    $this->configuration['device_id'] = $form_state->getValue('device_id');
  }

  /**
   * {@inheritdoc}
   */
  public function send(SmsMessageInterface $sms_message) {
    $key = $this->configuration['key'];
    $deviceId = (float) $this->configuration['device_id'];
    $recipient = $sms_message->getRecipients()[0];
    $result = new SmsMessageResult();
    $message_id = (isset($this->configuration['message_id'])) ? $this->configuration['message_id'] : rand(1000, 10000000);

    $report = new SmsDeliveryReport();
    $report->setRecipient($recipient);

    // Create the client.
    try {
      $numbers = $sms_message->getRecipients();
      $message = $sms_message->getMessage();
      $report->setMessageId($message_id);
      $config = Configuration::getDefaultConfiguration();
      $config->setApiKey('Authorization', $key);
      $apiClient = new ApiClient($config);
      $messageClient = new MessageApi($apiClient);
      $messages = [];
      foreach ($numbers as $nid => $number) {
        $messages[] = new SendMessageRequest([
          'phoneNumber' => $number,
          'message' => $message,
          'deviceId' => $deviceId,
        ]);
      }
      $action_result = $messageClient->sendMessages($messages);
      $report->setStatus(SmsMessageReportStatus::DELIVERED);
    }
    catch (\Exception $e) {
      $result->setError(SmsMessageResultStatus::ACCOUNT_ERROR);
      $report->setStatus(SmsMessageReportStatus::ERROR);
      $report->setStatusMessage($e->getMessage());
    }
    if ($report->getStatus()) {
      $result->addReport($report);
    }
    return $result;
  }

  /**
   * Process an incoming message POST request.
   *
   * This callback expects a 'messages' POST value containing JSON.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param \Drupal\sms\Entity\SmsGatewayInterface $sms_gateway
   *   The gateway instance.
   *
   * @return \Drupal\sms\SmsProcessingResponse
   *   A SMS processing response task.
   */
  public function processIncoming(Request $request, SmsGatewayInterface $sms_gateway) {
    $key = $this->configuration['key'];
    $request->getRequestFormat();
    $params = \GuzzleHttp\json_decode($request->getContent(), TRUE);
    // Get the message content.
    // $message_id = $params['id'];.
    if (!isset($params['id']) || empty($params['id'])) {

    }
    $config = Configuration::getDefaultConfiguration();

    $client = \Drupal::httpClient();
    $url = 'https://smsgateway.me/api/v4/message/' . $params['id'];
    $method = 'GET';
    $options = [
      'headers' => [
        'Authorization' => $key,
        'id' => $params['id'],
      ],
    ];
    $response = $client->request($method, $url, $options);
    $code = $response->getStatusCode();
    $body = '';
    if ($code == 200) {
      $body = $response->getBody()->getContents();
    }
    $post_params = \GuzzleHttp\json_decode($body, TRUE);
    $message_content = $post_params['message'];
    $this->logger->debug('Body content: ' . print_r($post_params, TRUE));

    // If no secret match, abort.
    if (empty($message_content)) {
      $this->logger->debug('Triggered with empty message parameter');
      $response = new Response('', 404);
      $task = (new SmsProcessingResponse())
        ->setResponse($response);
      return $task;
    }
    $result = new SmsMessageResult();

    $incoming_recipient = $post_params['phone_number'];
    $report = (new SmsDeliveryReport())
      ->setRecipient($incoming_recipient);
    $result->addReport($report);

    $message = (new SmsMessage())
      ->setDirection(Direction::INCOMING)
      ->setGateway($sms_gateway)
      ->setResult($result)
      ->setSenderNumber($post_params['phone_number'])
      ->setMessage($post_params['message'])
      ->addRecipient($post_params['phone_number']);
    $messages[] = $message;
    $response = new Response('', 204);
    $task = (new SmsProcessingResponse())
      ->setResponse($response)
      ->setMessages($messages);
    $this->logger->debug('Done with message.');
    return $task;
  }

}
